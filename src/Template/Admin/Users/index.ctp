<div class="section" id="page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <h1>
                            <i class="fa fa-tags"></i> PAINEL DO ADMINISTRADOR > <?= $type ?></h1>
                        <p>Aqui você pode gerenciar todos os usuários.</p>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="table-responsive">
                            <table id="users-table" class="table table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Nome</th>
                                        <th>Tel. Celular</th>
                                        <th>Email</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($users as $user): ?>
                                        <tr>
                                            <td style="padding-top: 12px;">
                                                <?php if ( $user->category === 'admin'): ?>
                                                    <span class="label label-default">
                                                        administrador
                                                    </span>
                                                <?php elseif( $user->category === 'personal'): ?>
                                                    <span class="label label-default">
                                                        particular
                                                    </span>
                                                <?php else: ?>
                                                    <span class="label label-default">
                                                        concessionária
                                                    </span>
                                                <?php endif ?>
                                            </td>
                                            <td>
                                                <?= $user->profile->name ?>
                                            </td>
                                            <td>
                                                <?= $user->profile ? $user->profile->cellphone_number : 'N/I' ?>
                                            </td>
                                            <td><?= $user->email ?></td>
                                            <td>
                                                <a 
                                                    class="btn btn-default btn-sm"
                                                    href="<?= \Cake\Routing\Router::url(['prefix' => 'admin', 'controller' => 'Users', 'action' => 'edit', $user->id]) ?>"
                                                >
                                                    <i class="fa fa-fw fa-edit"></i>
                                                </a>
                                                <button 
                                                    class="btn btn-danger btn-sm"
                                                    onclick="javascript:deleteUser(<?= $user->id ?>)">
                                                    <i class="fa fa-fw fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- ROW IN -->

                <form id="form-delete-user" method="post" action="<?= \Cake\Routing\Router::url(['prefix' => 'admin', 'controller' => 'Users', 'action' => 'delete']) ?>">
                </form>
            </div>
            <!-- PAINEL -->

            <?= $this->element('notifications') ?>
        </div>
    </div>
</div>
<!-- BOX PAGE -->

<?php $this->start('js') ?>
    <?= $this->Html->script('admin/controllers/users.js') ?>
<?php $this->end() ?>
