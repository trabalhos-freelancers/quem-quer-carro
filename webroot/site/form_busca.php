	<div class="section" id="busca_avancada_box">
<div class="bg_lent">
		<div class="container">
			<div class="row">
				
				<div class="col-xs-12 col-sm-12 col-md-12">
					<form action="" class="busca_form">
						<label class="quatro"><input type="text" placeholder="Palavra-chave"></label>
						<label class="quatroS"><input type="text" placeholder="Marca"></label>
						<label class="quatroS quatroSmob"><input type="text" placeholder="Modelo"></label>

						<label class="quatroS"><input type="text" placeholder="Ano"></label>

						<div class="variacao">
							<label for="">Valor</label>
							<label class="meioL"><input type="number" placeholder="Mínimo"></label>
							<label class="meioR"><input type="number" placeholder="Máximo"></label>
						</div>
						<!-- VARIAÇÃO -->

						<div class="variacaoTipo">
							<label for="">Tipo</label>
							<label>
							<select>
								<option value="">Particular</option>
								<option value="">Concessionária</option>
							</select>
							</label>
						</div>
						<!-- VARIAÇÃO -->

						<button value="" class="bav"><i class="fa fa-search"></i> BUSCAR</button>

						<button type="reset" value="" class="bavP">LIMPAR FILTROS</button>
					</form>
				</div>

			</div><!--ROW-->
		</div>
</div><!-- BG LENTE -->
	</div><!--BUSCA AVANÇADA-->