<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO USUÁRIO > INSERIR ANÚNCIO</h1>
							<p>Insira um novo anúncio agora mesmo!</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<form action="" id="formulario">
								<label><input type="text" placeholder="TÍTULO DO ANÚNCIO"></label>
								
								<h3 class="titulo_lab"><i class="fa fa-car"></i> INFORMAÇÕES DO VEÍCULO</h3>
								<label><input type="text" placeholder="MARCA"></label>
								<label><input type="text" placeholder="MODELO"></label>
								<label class="tresCol"><input type="text" placeholder="ANO"></label>
								<label class="tresCol"><input type="text" placeholder="MOTOR"></label>
								<label class="tresColR"><input type="text" placeholder="COR"></label>
								<label><input type="text" placeholder="COMBUSTÍVEL"></label>
								<label><input type="text" placeholder="VALOR"></label>
								<label class="cid"><input type="text" placeholder="KM"></label>
								<label class="uf"><input type="text" placeholder="PLACA"></label>

								<label><textarea placeholder="TEXTO"></textarea></label>
								

								<h3 class="titulo_lab">OPCIONAIS</h3>
								
								<div class="ops">
									<label><input type="checkbox"> Ar Condicionado</label>
									<label><input type="checkbox"> Vidro Elétrico</label>
									<label><input type="checkbox"> Airbag</label>
									<label><input type="checkbox"> Alarme</label>
									<!-- deixar opção no painel do admin master para inserir mais opcionais -->
								</div>
								<!-- BOX OPCIONIS -->

								<h3 class="titulo_lab">CARREGAR FOTOS</h3>
								<label><input type="file" placeholder=""></label>
								
								<div class="previa_fotos">
									<a href=""><img src="img/foto_car.jpg" alt=""></a>
									<a href=""><img src="img/foto_car.jpg" alt=""></a>
									<a href=""><img src="img/foto_car.jpg" alt=""></a>
									<a href=""><img src="img/foto_car.jpg" alt=""></a>
									<a href=""><img src="img/foto_car.jpg" alt=""></a>
									<a href=""><img src="img/foto_car.jpg" alt=""></a>
								</div>
								<!-- PREVIA DE FOTOS -->
												
							
								<button onclick="window.location.href='#'"><i class="fa fa-save"></i> SALVAR COMO RASCUNHO</button>

								<button onclick="window.location.href='#'"><i class="fa fa-edit"></i> PUBLICAR ANUNCIO</button>
								
								<label class="afirmo"><input type="checkbox"> Afirmo que todas as informações fornecidas por mim são verdadeiras.</label>
							</form>
							
						</div>
					</div><!-- ROW IN -->
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
