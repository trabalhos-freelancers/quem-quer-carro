<?php
namespace App\Controller\Authentication;

use App\Controller\AppController;

class LoginController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->layout = 'login';
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }

        $this->render('/Authentication/login');
    }
}
