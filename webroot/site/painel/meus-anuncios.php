<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO USUÁRIO > MEUS ANUNCIOS</h1>
							<p>Aqui você pode gerenciar seus anuncios.</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<div class="row">

							<!-- INICIO DO LOOP -->
							<div class="loop_anuncios_painel">

								<div class="col-xs-12 col-sm-2 col-md-2">
									<img src="img/foto_car.jpg" alt="" class="center-block img-responsive">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-6">
									<p><strong>Título do Anuncio</strong></p>
									<p><i>Desde: 14 de Junho de 2018</i></p>
								</div>

								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Excluir"><i class="fa fa-trash"></i></button>
								</div>
								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Editar"><i class="fa fa-edit"></i></button>
								</div>
							</div>
							<!-- FIM DO LOOP -->

							<!-- INICIO DO LOOP -->
							<div class="loop_anuncios_painel">

								<div class="col-xs-12 col-sm-2 col-md-2">
									<img src="img/foto_car.jpg" alt="" class="center-block img-responsive">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-6">
									<p><strong>Título do Anuncio</strong></p>
									<p><i>Desde: 14 de Junho de 2018</i></p>
								</div>

								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Excluir"><i class="fa fa-trash"></i></button>
								</div>
								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Editar"><i class="fa fa-edit"></i></button>
								</div>
							</div>
							<!-- FIM DO LOOP -->

							<!-- INICIO DO LOOP -->
							<div class="loop_anuncios_painel">

								<div class="col-xs-12 col-sm-2 col-md-2">
									<img src="img/foto_car.jpg" alt="" class="center-block img-responsive">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-6">
									<p><strong>Título do Anuncio</strong></p>
									<p><i>Desde: 14 de Junho de 2018</i></p>
								</div>

								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Excluir"><i class="fa fa-trash"></i></button>
								</div>
								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Editar"><i class="fa fa-edit"></i></button>
								</div>
							</div>
							<!-- FIM DO LOOP -->

							<!-- INICIO DO LOOP -->
							<div class="loop_anuncios_painel">

								<div class="col-xs-12 col-sm-2 col-md-2">
									<img src="img/foto_car.jpg" alt="" class="center-block img-responsive">
								</div>

								<div class="col-xs-12 col-sm-8 col-md-6">
									<p><strong>Título do Anuncio</strong></p>
									<p><i>Desde: 14 de Junho de 2018</i></p>
								</div>

								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Excluir"><i class="fa fa-trash"></i></button>
								</div>
								<div class="col-xs-6 col-sm-2 col-md-2">
									<button title="Editar"><i class="fa fa-edit"></i></button>
								</div>
							</div>
							<!-- FIM DO LOOP -->

							</div>
						</div>
					</div><!-- ROW IN -->

					<div class="col-xs-12 col-sm-12 col-md-12 text-center">
					<nav aria-label="Page navigation">
					  <ul class="pagination">
					    <li>
					      <a href="#" aria-label="Previous">
					        <span aria-hidden="true">&laquo;</span>
					      </a>
					    </li>
					    <li><a href="#">1</a></li>
					    <li><a href="#">2</a></li>
					    <li><a href="#">3</a></li>
					    <li><a href="#">4</a></li>
					    <li><a href="#">5</a></li>
					    <li>
					      <a href="#" aria-label="Next">
					        <span aria-hidden="true">&raquo;</span>
					      </a>
					    </li>
					  </ul>
					</nav>
				</div>
				<!-- PAGINAÇÃO -->
				
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
