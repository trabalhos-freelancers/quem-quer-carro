<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AdvertsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AdvertsTable Test Case
 */
class AdvertsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AdvertsTable
     */
    public $Adverts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.adverts',
        'app.users',
        'app.images'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Adverts') ? [] : ['className' => AdvertsTable::class];
        $this->Adverts = TableRegistry::getTableLocator()->get('Adverts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Adverts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
