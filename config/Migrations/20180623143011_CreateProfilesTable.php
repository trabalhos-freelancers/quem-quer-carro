<?php
use Migrations\AbstractMigration;

class CreateProfilesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('profiles');
        $table->addColumn('user_id', 'integer');
        $table->addColumn('name', 'string', ['null' => true]);
        $table->addColumn('nickname', 'string', ['null' => true]);
        $table->addColumn('zipcode', 'string', ['null' => true, 'limit' => 12]);
        $table->addColumn('address', 'string', ['null' => true]);
        $table->addColumn('neighborhood', 'string', ['null' => true]);
        $table->addColumn('city', 'string', ['null' => true, 'limit' => '100']);
        $table->addColumn('state', 'string', ['null' => true, 'limit' => 2]);
        $table->addColumn('phone_number', 'string', ['limit' => 35, 'null' => true]);
        $table->addColumn('cellphone_number', 'string', ['limit' => 35, 'null' => true]);
        $table->addColumn('email', 'string', ['null' => true]);
        $table->addColumn('created', 'datetime');
        $table->addColumn('updated', 'datetime', ['null' => true]);
        
        $table->addIndex(['email'], ['unique' => true]);
        $table->addForeignKey('user_id', 'users', ['id']);

        $table->create();
    }
}
