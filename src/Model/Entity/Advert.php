<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Advert Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $slug
 * @property string $title
 * @property string $brand
 * @property string $model
 * @property int $year
 * @property string $engine
 * @property string $color
 * @property string $fuel
 * @property float $value
 * @property int $km
 * @property string $plate
 * @property string $resume
 * @property bool $air_conditioner
 * @property bool $eletric_glass
 * @property bool $airbag
 * @property bool $alarm
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Image[] $images
 */
class Advert extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'slug' => true,
        'title' => true,
        'brand' => true,
        'model' => true,
        'year' => true,
        'engine' => true,
        'color' => true,
        'fuel' => true,
        'value' => true,
        'km' => true,
        'plate' => true,
        'resume' => true,
        'air_conditioner' => true,
        'eletric_glass' => true,
        'airbag' => true,
        'alarm' => true,
        'created' => true,
        'updated' => true,
        'user' => true,
        'images' => true
    ];
}
