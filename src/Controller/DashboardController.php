<?php
namespace App\Controller;

use App\Controller\AppController;

class DashboardController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function dashboard()
    {
        switch ($this->Auth->user('category')) {
            case 'admin':
                $this->redirect('/admin');
                break;

            case 'personal':
                $this->redirect('/personal');
                break;

            case 'dealership':
                $this->redirect('/dealership');
                break;
        }
    }
}
