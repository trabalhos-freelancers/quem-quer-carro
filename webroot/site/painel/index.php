<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO USUÁRIO</h1>
							<p>Bem vindo à área de clientes pessoa física!</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<h2>Veja como é fácil anunciar seu veículo!</h2>
							<iframe width="100%" height="415" src="https://www.youtube.com/embed/1E5WTK-76BU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
						</div>
					</div><!-- ROW IN -->
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
