<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO USUÁRIO > PERFIL</h1>
							<p>Mantenha seus dados atualizados!</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<ul class="info_perfil">
								<li><strong>NOME:</strong> HÉRCULES MOURA</li>
								<li><strong>NOME PARA EXIBIÇÃO:</strong> GALEGO</li>
								<li><strong>ENDEREÇO:</strong> VILA NOVA DA RAINHA</li>
								<li><strong>BAIRRO:</strong> CENTRO</li>
								<li><strong>CIDADE:</strong> CAMPINA GRANDE - PB</li>
								<li><strong>TELEFONE:</strong> (83) 0000.0000</li>
								<li><strong>CELULAR:</strong> (83) 0000.0000</li>
								<li><strong>EMAIL:</strong> nome@nome.com</li>

								<li><strong>NOME DE USUÁRIO:</strong> hercules</li>
								<li><strong>SENHA:</strong> *****</li>
							</ul>

							<button onclick="window.location.href='perfil-editar.php'"><i class="fa fa-edit"></i> EDITAR PERFIL</button>
						</div>
					</div><!-- ROW IN -->
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
