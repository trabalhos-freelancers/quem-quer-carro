<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO ADMINISTRADOR > INSERIR NOVO USUÁRIO</h1>
							<p>Confira todas as informações em clique em salvar!</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<form action="" id="formulario">
								<div class="ops">
									<label><input type="checkbox"> Adminstrador</label>
									<label><input type="checkbox"> Particular</label>
									<label><input type="checkbox"> Concessionárias</label>
								</div>
								<!-- BOX OPCIONIS -->
								
								<h3 class="titulo_lab"><i class="fa fa-user"></i> INFORMAÇÕES DO USUÁRIO</h3>
								<label><input type="text" placeholder="NOME COMPLETO"></label>
								<label><input type="text" placeholder="COMO DESEJA SER CHAMADO"></label>
								<label><input type="text" placeholder="RUA"></label>
								<label><input type="text" placeholder="BAIRRO"></label>
								<label class="cid"><input type="text" placeholder="CIDADE"></label>
								<label class="uf"><input type="text" placeholder="ESTADO"></label>
								<label><input type="email" placeholder="E-MAIL"></label>
								<label class="mL"><input type="text" placeholder="TELEFONE"></label>
								<label class="mR"><input type="text" placeholder="CELULAR"></label>

								<h3 class="titulo_lab"><i class="fa fa-unlock-alt"></i> INFORMAÇÕES DE ACESSO</h3>
								<label><input type="text" placeholder="USUÁRIO"></label>
								<label><input type="password" placeholder="SENHA"></label>
								<label><input type="password" placeholder="CONFIRME A SENHA"></label>

								<label><input type="checkbox"> Afirmo que todas as informações fornecidas por mim são verdadeiras.</label>

								<button><i class="fa fa-save"></i> INSERIR / SALVAR</button>
							</form>
						</div>
					</div><!-- ROW IN -->
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
