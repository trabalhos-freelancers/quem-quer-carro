<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12" id="t_anuncio">
					<h1><i class="fa fa-car"></i> Título do Anúncio</h1>
					<p>1.0 COMFORT STYLE 12V FLEX 4P MANUAL 2013/2013</p>
				</div>
				<!-- TITULO -->

				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<?php include('slide_anuncio/slide_anuncio.php');?>
						</div><!--SLIDE ANUNCIO-->

						<div class="col-xs-12 col-sm-12 col-md-12" id="info_anuncio_box">
							<h2>OBSERVAÇÕES</h2>
							<p class="info_carro">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quasi quae repellendus blanditiis quas voluptates quidem minus voluptate consequatur reprehenderit, inventore eos deleniti velit ab, soluta sed dicta culpa quaerat neque.
							</p>

							<ul class="info_carro">
								<li><strong>Marca:</strong> Chevrolet </li>
								<li><strong>Modelo:</strong> Onix </li>
								<li><strong>Ano:</strong> 2019 </li>
								<li><strong>Motor:</strong> 1.0 </li>
								<li><strong>Cor:</strong> Prata </li>
								<li><strong>Combustível:</strong> Gasolina </li>
								<li><strong>Km:</strong> 0000 </li>
								<li><strong>Valor:</strong> R$19.900 </li>
							</ul>
							


							<h3>OPCIONAIS</h3>
							<p class="info_carro">
								Ar Condicionado, Airbag, Vidro Elétrico, Alarme
							</p>

						</div><!-- INFO VEICULO -->

					</div>
					<!-- ROW IN SIDEBAR L -->
				</div>
				<!-- SIDEBAR L -->			


				<div class="col-xs-12 col-sm-4 col-md-4" id="sidebar">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12" id="info_vendedor">
							<h3>R$19.900</h3>

							<form action="" id="form_anuncio_mens">
								<label><input type="text" placeholder="SEU NOME"></label>
								<label><input type="text" placeholder="E-MAIL"></label>
								<label><input type="text" placeholder="TELEFONE"></label>
								<label><textarea placeholder="SUA MENSAGEM">Olá, tenho interesse no veículo. Por favor entre em contato.</textarea></label>
								<label class="receb_info"><input type="checkbox"> Receber informações?</label>
								<button>ENVIAR MENSAGEM</button>
							</form>
						</div><!-- FORMULARIO -->

						<div class="col-xs-12 col-sm-12 col-md-12" id="widget_sid">
							<span>GOSTOU? ENTRE EM CONTATO!</span>
							 
							<p><strong>Horário de funcionamento</strong> </p>
							<p>Segunda à Sexta das 8h às 17:30h</p>
							
							<button onclick="window.location.href='https://api.whatsapp.com/send?phone=5583988139291&text=Olá, estou interessado no Veículo (Nome do Veículo) + (URL do Anuncio)'" class="whats"><i class="fa fa-whatsapp"></i> Nos chame no WhatsApp</button>
						</div><!-- WIDGET -->

						<div class="col-xs-12 col-sm-12 col-md-12" id="widget_sid_pub">
							<img src="img/banner.gif" class="center-block img-responsive" alt="">
							
						</div><!-- WIDGET -->

					</div>
					<!-- ROW SIDEBAR -->
				</div>
				<!-- SIDEBAR -->				

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->


<?php
include('footer.php');
?>
