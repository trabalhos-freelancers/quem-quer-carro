<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Routing\Router;

/**
 * MainMenu cell
 */
class MainMenuCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
    }

    /**
     * Admin display method
     * 
     * @return void
     */
    public function admin()
    {
        $menu = [
            'HOME' => '/',
            'CONFIGURAÇÕES' => '#',
            'PUBLICIDADES' => '#',
            'PERFIL' => Router::url([
                'prefix' => 'admin',
                'controller' => 'Users',
                'action' => 'profile',
            ]),
            'TODOS OS ANÚNCIOS' => '#',
            'INSERIR ANÚNCIO' => '#',
            'GERENCIAR USUÁRIOS' => [
                'TODOS' => Router::url([
                    'prefix' => 'admin',
                    'controller' => 'Users',
                    'action' => 'index',
                ]),
                'PARTICULAR' => Router::url([
                    'prefix' => 'admin',
                    'controller' => 'Users',
                    'action' => 'index',
                    '?' => ['type' => 'personal'],
                ]),
                'CONCESSIONÁRIA' => Router::url([
                    'prefix' => 'admin',
                    'controller' => 'Users',
                    'action' => 'index',
                    '?' => ['type' => 'dealership'],
                ]),
                'CADASTRAR' => Router::url([
                    'prefix' => 'admin',
                    'controller' => 'Users',
                    'action' => 'add',
                ]),
            ],
            'SUPORTE' => '#',
            'SAIR' => '/logout'
        ];

        $this->set(compact('menu'));
    }

    /**
     * Personal display method
     * 
     * @return void
     */
    public function personal()
    {
        $menu = [
            'HOME' => '#',
            'CONFIGURAÇÕES' => '#',
            'PUBLICIDADES' => '#',
            'PERFIL' => '#',
            'TODOS OS ANÚNCIOS' => '#',
            'INSERIR ANÚNCIO' => '#',
            'GERENCIAR USUÁRIOS' => [
                'PARTICULAR' => '#',
                'CONCESSIONÁRIA' => '#',
                'CADASTRAR' => '#'
            ]
        ];

        $this->set(compact('menu'));
    }

    /**
     * Dealership display method
     * 
     * @return void
     */
    public function dealership()
    {
        $menu = [
            'HOME' => '#',
            'CONFIGURAÇÕES' => '#',
            'PUBLICIDADES' => '#',
            'PERFIL' => '#',
            'TODOS OS ANÚNCIOS' => '#',
            'INSERIR ANÚNCIO' => '#',
            'GERENCIAR USUÁRIOS' => [
                'PARTICULAR' => '#',
                'CONCESSIONÁRIA' => '#',
                'CADASTRAR' => '#'
            ]
        ];

        $this->set(compact('menu'));
    }
}
