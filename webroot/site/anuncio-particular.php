<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h1><i class="fa fa-tags"></i> PLANOS PARA PARTICULARES</h1>
					<p>Bem vindo à área de clientes pessoa física! <br> <br>
Aqui você descobrirá como se cadastrar no nosso site, ter acesso à sua área privativa e anunciar seu(s) veículo(s).</p>
				</div>
				<!-- TITULO -->

				<div class="col-xs-12 col-sm-4 col-md-4 plano-loop">
					<div class="bg-plus">
						<h2><i class="fa fa-tags"></i> PLANO PLUS</h2>
					<p class="valor">R$39,90</p>
					<p>- Até 6 fotos; <br>
 (as fotos do veículo são obrigatórias)
<br><br>
- Visualizado por qualquer internauta 
que acesse o QuemQuerCarro e 
utilize as ferramentas de busca;<br><br>

- Liberação para visualização em no
 máximo 48 horas após confirmação
 do pagamento.
<br><br>
- 45 dias de publicação
 do anuncio no site. 
(após a confirmação de pagamento)</p>
					<p class="text-center"><img src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/120x53-pagar-laranja.gif" alt=""></p>
					</div><!-- BG -->
				</div>
				<!-- LOOP PLANOS -->

				<div class="col-xs-12 col-sm-4 col-md-4 plano-loop">
					<div class="bg-star">
					<h2><i class="fa fa-star"></i> PLANO STAR</h2>
					<p class="valor">R$49,90</p>
					<p>- Até 6 fotos; <br>
 (as fotos do veículo são obrigatórias)
<br><br>
- Visualizado por qualquer internauta 
que acesse o QuemQuerCarro e 
utilize as ferramentas de busca;
<br><br>
- Liberação para visualização em no
 máximo 48 horas após confirmação
 do pagamento.
<br><br>
- 60 dias de publicação
 do anuncio no site. 
(após a confirmação de pagamento)</p>
					<p class="text-center"><img src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/120x53-pagar-laranja.gif" alt=""></p>
					</div><!-- BG -->
				</div>
				<!-- LOOP PLANOS -->

				<div class="col-xs-12 col-sm-4 col-md-4 plano-loop">
					<div class="bg-gold">
					<h2><i class="fa fa-dollar"></i> PLANO GOLD</h2>
					<p class="valor">R$89,90</p>
					<p>- Até 6 fotos;<br>
 (as fotos do veículo são obrigatórias)
<br><br>
- Visualizado por qualquer internauta 
que acesse o QuemQuerCarro e 
utilize as ferramentas de busca;
<br><br>
- Liberação para visualização em no
 máximo 48 horas após confirmação
 do pagamento.
<br><br>
- Publicação do anuncio no 
site até a venda do veículo. 
(após a confirmação de pagamento)</p>
					<p class="text-center"><img src="https://p.simg.uol.com.br/out/pagseguro/i/botoes/pagamentos/120x53-pagar-laranja.gif" alt=""></p>
					</div><!-- BG -->
				</div>
				<!-- LOOP PLANOS -->

				

				<div class="col-xs-12 col-sm-12 col-md-12" id="info_anunciar">
					* Os anúncios serão liberados em até 48 horas úteis após a confirmação do pagamento pelo PagSeguro.

				</div>
				<!-- TEXTO 2 -->

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
