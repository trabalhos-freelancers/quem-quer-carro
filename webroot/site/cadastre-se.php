<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h1><i class="fa fa-tags"></i> CADASTRE-SE</h1>
					<p>Insira os dados solicitados abaixo para seu cadastro no QuemQuerCarro.</p>
				</div>
				<!-- TITULO -->

				<div class="col-xs-12 col-sm-6 col-md-6">
					<form action="" id="formulario">
						<h3 class="titulo_lab"><i class="fa fa-user"></i> INFORMAÇÕES PESSOAIS</h3>
						<label><input type="text" placeholder="NOME COMPLETO"></label>
						<label><input type="text" placeholder="COMO DESEJA SER CHAMADO"></label>
						<label><input type="text" placeholder="RUA"></label>
						<label><input type="text" placeholder="BAIRRO"></label>
						<label class="cid"><input type="text" placeholder="CIDADE"></label>
						<label class="uf"><input type="text" placeholder="ESTADO"></label>
						<label><input type="email" placeholder="E-MAIL"></label>
						<label class="mL"><input type="text" placeholder="TELEFONE"></label>
						<label class="mR"><input type="text" placeholder="CELULAR"></label>

						<h3 class="titulo_lab"><i class="fa fa-unlock-alt"></i> INFORMAÇÕES DE ACESSO</h3>
						<label><input type="text" placeholder="USUÁRIO"></label>
						<label><input type="password" placeholder="SENHA"></label>
						<label><input type="password" placeholder="CONFIRME A SENHA"></label>

						<label><input type="checkbox"> Afirmo que todas as informações fornecidas por mim são verdadeiras.</label>

						<button>CADASTRAR</button>
					</form>
				</div>
				<!-- FORMULARIO -->

				<div class="col-xs-12 col-sm-6 col-md-6">
					<strong>TELEFONES</strong>
					(83) 9305-9208 - Claro <br>
					(83) 8129-0959 - Vivo <br>
					(83) 9915-9302 - TIM <br>
 

				ou envie um e-mail para contato@quemquercarro.com.br e entre em contato.
				</div>
				<!-- INFO CONTATO -->

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
