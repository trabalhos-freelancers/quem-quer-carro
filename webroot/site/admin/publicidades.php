<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO ADMINISTRADOR > PUBLICIDADES</h1>
							<p>Aqui voê pode gerenciar os banners do site.</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<form action="" id="formulario">
								
								<label><input type="text" placeholder="Titulo"></label>
								
								

								<h3 class="titulo_lab">CATEGORIA</h3>
								
								<div class="ops">
									<label><input type="checkbox"> Home</label>
									<label><input type="checkbox"> Lateral 1</label>
									<label><input type="checkbox"> Lateral 2</label>
									<label><input type="checkbox"> Lateral 3</label>
									<!-- deixar opção no painel do admin master para inserir mais opcionais -->
								</div>
								<!-- BOX OPCIONIS -->

								<h3 class="titulo_lab">CARREGAR IMAGEM</h3>
								<label><input type="file" placeholder=""></label>
								
								<div class="previa_fotos">
									<a href=""><img src="img/foto_car.jpg" alt=""></a>
								</div>
								<!-- PREVIA DE FOTOS -->

								<label><input type="text" placeholder="Url de destino"></label>
								<label><textarea placeholder="Incorporar Código"></textarea></label>
												
							
								<button onclick="window.location.href='#'"><i class="fa fa-save"></i> SALVAR COMO RASCUNHO</button>

								<button onclick="window.location.href='#'"><i class="fa fa-edit"></i> PUBLICAR</button>
							
							</form>
							
						</div>
					</div><!-- ROW IN -->
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
