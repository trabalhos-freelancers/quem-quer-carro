<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Profile Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $nickname
 * @property string $zipcode
 * @property string $address
 * @property string $neighborhood
 * @property string $city
 * @property string $state
 * @property string $phone_number
 * @property string $cellphone_number
 * @property string $email
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $updated
 *
 * @property \App\Model\Entity\User $user
 */
class Profile extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'name' => true,
        'nickname' => true,
        'zipcode' => true,
        'address' => true,
        'neighborhood' => true,
        'city' => true,
        'state' => true,
        'phone_number' => true,
        'cellphone_number' => true,
        'email' => true,
        'created' => true,
        'updated' => true,
        'user' => true
    ];
}
