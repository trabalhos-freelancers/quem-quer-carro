<div class="section" id="page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <h1>
                            <i class="fa fa-tags"></i> 
                            PAINEL DO ADMINISTRADOR > INSERIR NOVO USUÁRIO
                        </h1>
                        <p>Confira todas as informações em clique em salvar!</p>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
                        <form 
                            id="formulario" 
                            action="<?= \Cake\Routing\Router::url(['controller' => 'Users', 'action' => 'add', 'prefix' => 'admin'])?>" 
                            method="POST"
                        >
                            <div class="ops">
                                <label>
                                    <input type="checkbox" name="category" value="admin"> Adminstrador
                                </label>

                                <label>
                                    <input type="checkbox" name="category" value="personal"> Particular
                                </label>
                                
                                <label>
                                    <input type="checkbox" name="category" value="dealership"> Concessionárias
                                </label>
                            </div>
                            <!-- BOX OPCIONIS -->

                            <h3 class="titulo_lab">
                                <i class="fa fa-user"></i> INFORMAÇÕES DO USUÁRIO</h3>
                            <label>
                                <input type="text" name="name" placeholder="NOME COMPLETO" required>
                            </label>
                            <label>
                                <input type="text" name="nickname" placeholder="COMO DESEJA SER CHAMADO">
                            </label>
                            <label>
                                <input type="text" name="address" placeholder="RUA" required>
                            </label>
                            <label>
                                <input type="text" name="neighborhood" placeholder="BAIRRO" required>
                            </label>
                            <label class="cid">
                                <input type="text" name="city" placeholder="CIDADE" required>
                            </label>
                            <label class="uf">
                                <input type="text" name="state" placeholder="ESTADO" required>
                            </label>
                            <label>
                                <input type="email" name="email" placeholder="E-MAIL" required>
                            </label>
                            <label class="mL">
                                <input type="text" name="phone_number" placeholder="TELEFONE" required>
                            </label>
                            <label class="mR">
                                <input type="text" name="cellphone_number" placeholder="CELULAR" required>
                            </label>

                            <h3 class="titulo_lab">
                                <i class="fa fa-unlock-alt"></i> INFORMAÇÕES DE ACESSO</h3>
                            <label>
                                <input type="email" name="email" placeholder="EMAIL" required>
                            </label>
                            <label>
                                <input type="password" name="password" placeholder="SENHA" required>
                            </label>
                            <label>
                                <input type="password" name="password_confirmation" placeholder="CONFIRME A SENHA" required>
                            </label>

                            <label>
                                <input type="checkbox"> Afirmo que todas as informações fornecidas por mim são verdadeiras.
                            </label>

                            <button type="submit">
                                <i class="fa fa-save"></i> 
                                INSERIR / SALVAR
                            </button>
                        </form>
                    </div>
                </div>
                <!-- ROW IN -->
            </div>
            <!-- PAINEL -->

            <?= $this->element('notifications') ;?>
        </div>
    </div>
</div>
<!-- BOX PAGE -->
