<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO ADMINISTRADOR > SUPORTE</h1>
							<p>Precisa de ajuda? </p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<h2>PERGUNTAS FREQUENTES</h2>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Quantos Anúncios posso publicar?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        Você pode ter quantos anuncios desejar...
      </div>
    </div>
  </div>
  <!-- LOOP PERFUNTAS FREQUENTES -->

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Como faço o pagamento do meu plano?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
        Você receberá uma notificação 5 dias antes do vencimento.
      </div>
    </div>
  </div>
<!-- LOOP PERFUNTAS FREQUENTES -->

  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingThree">
      <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          Meu anúncio é aprovado automaticamente?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        Sim! seu anúncio será aprovado automaticamente logo após o envio das informações do seu veículo.
      </div>
    </div>
  </div>
  <!-- LOOP PERFUNTAS FREQUENTES -->
</div>
<!-- ACORDION -->



							AINDA TEM DÚVIDAS?
							<button onclick="window.location.href='perfil-editar.php'"><i class="fa fa-whatsapp"></i> CHAMAR NO WHASTSAPP </button>
						</div>
					</div><!-- ROW IN -->
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
