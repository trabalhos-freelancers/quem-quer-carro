<div class="section" id="page">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <h1>
                            <i class="fa fa-tags"></i> 
                            PAINEL DO ADMINISTRADOR > EDITAR USUÁRIO
                        </h1>
                        <p>Confira todas as informações em clique em salvar!</p>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
                        <form 
                            id="formulario" 
                            action="<?= \Cake\Routing\Router::url(['controller' => 'Users', 'action' => 'edit', 'prefix' => 'admin', $user->id])?>" 
                            method="POST"
                        >
                            <div class="ops">
                                <label>
                                    <input 
                                        type="checkbox" 
                                        name="category" 
                                        value="admin"
                                        <?php if ($user->category === 'admin'): ?>
                                            checked
                                        <?php endif ?>
                                    > Adminstrador
                                </label>

                                <label>
                                    <input 
                                        type="checkbox" 
                                        name="category" 
                                        value="personal"
                                        <?php if ($user->category === 'personal'): ?>
                                            checked
                                        <?php endif ?>
                                    > Particular
                                </label>
                                
                                <label>
                                    <input 
                                        type="checkbox" 
                                        name="category" 
                                        value="dealership"
                                        <?php if ($user->category === 'dealership'): ?>
                                            checked
                                        <?php endif ?>
                                    > Concessionárias
                                </label>
                            </div>
                            <!-- BOX OPCIONIS -->

                            <h3 class="titulo_lab">
                                <i class="fa fa-user"></i> INFORMAÇÕES DO USUÁRIO</h3>
                            <label>
                                <input type="text" name="name" placeholder="NOME COMPLETO" value="<?= $user->profile->name ?>">
                            </label>
                            <label>
                                <input type="text" name="nickname" placeholder="COMO DESEJA SER CHAMADO" value="<?= $user->profile->nickname ?>">
                            </label>
                            <label>
                                <input type="text" name="address" placeholder="RUA" value="<?= $user->profile->address ?>">
                            </label>
                            <label>
                                <input type="text" name="neighborhood" placeholder="BAIRRO" value="<?= $user->profile->neighborhood ?>">
                            </label>
                            <label class="cid">
                                <input type="text" name="city" placeholder="CIDADE" value="<?= $user->profile->city ?>">
                            </label>
                            <label class="uf">
                                <input type="text" name="state" value="<?= $user->profile->state ?>">
                            </label>
                            <label>
                                <input type="email" name="email" placeholder="E-MAIL" value="<?= $user->profile->email ?>">
                            </label>
                            <label class="mL">
                                <input type="text" name="phone_number" placeholder="TELEFONE" value="<?= $user->profile->phone_number ?>">
                            </label>
                            <label class="mR">
                                <input type="text" name="cellphone_number" placeholder="CELULAR" value="<?= $user->profile->cellphone_number ?>">
                            </label>

                            <h3 class="titulo_lab">
                                <i class="fa fa-unlock-alt"></i> INFORMAÇÕES DE ACESSO</h3>
                            <label>
                                <input type="email" name="email" placeholder="EMAIL" value="<?= $user->email ?>">
                            </label>
                            <label>
                                <input type="password" name="password" placeholder="SENHA">
                            </label>
                            <label>
                                <input type="password" name="password_confirmation" placeholder="CONFIRME A SENHA">
                            </label>

                            <label>
                                <input type="checkbox"> Afirmo que todas as informações fornecidas por mim são verdadeiras.
                            </label>

                            <button type="submit">
                                <i class="fa fa-save"></i> 
                                INSERIR / SALVAR
                            </button>
                            <button type="button" onclick="javascript:history.back()">
                                <i class="fa fa-close"></i> 
                                CANCELAR
                            </button>
                        </form>
                    </div>
                </div>
                <!-- ROW IN -->
            </div>
            <!-- PAINEL -->

            <?= $this->element('notifications') ;?>
        </div>
    </div>
</div>
<!-- BOX PAGE -->
