<?php
namespace App\Controller\Authentication;

use App\Controller\AppController;

class LogoutController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
}
