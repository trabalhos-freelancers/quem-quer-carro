<nav id="menu">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-topo">
                        <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                        <span class="sr-only">MENU</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="menu-topo">
                    <ul id="" class="nav navbar-nav navbar-center">
                        <li>
                            <a href="home-admin.php">HOME</a>
                        </li>
                        <li >
                            <a href="configuracoes.php">CONFIGURAÇÕES</a>
                        </li>
                        <li>
                            <a href="publicidades.php">PUBLICIDADES</a>
                        </li>
                        <li>
                            <a href="perfil.php">PERFIL</a>
                        </li>
                        <li>
                            <a href="todos-anuncios.php">TODOS OS ANUNCIOS</a>
                        </li>
                        <li>
                            <a href="add-anuncio.php">INSERIR ANÚNCIO</a>
                        </li>
                        <li class="dropdown" data-toggle="dropdown">
                            <a href="#" 
                                class="dropdown-toggle" 
                                data-toggle="dropdown" 
                                role="button" 
                                aria-haspopup="true" 
                                aria-expanded="false"
                            >
                                GERENCIAR USUÁRIOS
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li onclick="window.location.href='usuarios-particular.php'">
                                    <a>PARTICULAR</a>
                                </li>
                                <li onclick="window.location.href='usuarios-concessionarias.php'">
                                    <a>CONCESSIONÁRIAS</a>
                                </li>
                                <li onclick="window.location.href='add-usuario.php'">
                                    <a>CADASTRAR</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="suporte.php">SUPORTE</a>
                        </li>
                        <li>
                            <a href="/logout">SAIR</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
