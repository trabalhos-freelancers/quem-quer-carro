<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


<title>Quem quer Carro</title>

<link href="style-painel.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

<!-- Bootstrap Core CSS -->
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../css/font-awesome.css" rel="stylesheet">

<!-- jQuery -->
<script src="../js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../js/bootstrap.min.js"></script>

</head>
<body>
	<header class="section" id="head">
		<div class="container">
			<div class="row">
				
				<div class="col-xs-6 col-sm-4 col-md-4">
					<a href="home-admin.php">
					<img src="../img/logo.png" class="center-block img-responsive" alt="Quem quer carro?"></a>
				</div><!--LOGO-->

				<div class="col-xs-12 col-sm-8 col-md-8 text-right" id="saudacao">
					<i class="fa fa-user"></i> Você está logado como <strong>Hércules Moura</strong>
	
				</div><!---->

			</div>
		</div>
	
	</header><!--HEAD-->

	<nav id="menu">
		<div class="container">
			<div class="row">
				
				<div class="col-xs-12 col-sm-12 col-md-12">

		 <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-topo">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
            <span class="sr-only">MENU</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         
        </div>

        <div class="collapse navbar-collapse" id="menu-topo">
        	<ul id="" class="nav navbar-nav navbar-center">
        		<li class="active"><a href="home-admin.php">HOME</a></li>
        		<li class="active"><a href="configuracoes.php">CONFIGURAÇÕES</a></li>
        		<li class="active"><a href="publicidades.php">PUBLICIDADES</a></li>
				<li><a href="perfil.php">PERFIL</a></li>
				<li><a href="todos-anuncios.php">TODOS OS ANUNCIOS</a></li>
				<li><a href="add-anuncio.php">INSERIR ANÚNCIO</a></li>
				<li class="dropdown" data-toggle="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">GERENCIAR USUÁRIOS <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li onclick="window.location.href='usuarios-particular.php'"><a >PARTICULAR</a></li>
						<li onclick="window.location.href='usuarios-concessionarias.php'"><a>CONCESSIONÁRIAS</a></li>
						<li onclick="window.location.href='add-usuario.php'"><a>CADASTRAR</a></li>
					</ul>
				</li>

				<li><a href="suporte.php">SUPORTE</a></li>
				<li><a href="#">SAIR</a></li>
        	</ul>

     
       </div>

   </div>
</div>
</div>
	</nav>
	<!-- MENU -->
	
