<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<h1><i class="fa fa-envelope"></i> FALE CONOSCO</h1>
					<p>Preencha o formulário</p>
				</div>
				<!-- TITULO -->

				<div class="col-xs-12 col-sm-6 col-md-6">
					<form action="" id="formulario">
						<label><input type="text" placeholder="NOME"></label>
						<label><input type="email" placeholder="E-MAIL"></label>
						<label><input type="text" placeholder="TELEFONE COMERCIAL"></label>
						<label><input type="text" placeholder="CELULAR"></label>
						<label><input type="text" placeholder="ASSUNTO"></label>
						<label><textarea placeholder="MENSAGEM"></textarea></label>

						<button>ENVIAR MENSAGEM</button>
					</form>
				</div>
				<!-- FORMULARIO -->

				<div class="col-xs-12 col-sm-6 col-md-6">
					<strong>TELEFONES</strong>
					(83) 9305-9208 - Claro <br>
					(83) 8129-0959 - Vivo <br>
					(83) 9915-9302 - TIM <br>
 

				ou envie um e-mail para contato@quemquercarro.com.br e entre em contato.
				</div>
				<!-- INFO CONTATO -->

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
