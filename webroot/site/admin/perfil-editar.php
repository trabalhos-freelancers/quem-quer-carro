<?php
include('head.php');
?>
	<div class="section" id="page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-md-8">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<h1><i class="fa fa-tags"></i> PAINEL DO ADMINISTRADOR > EDITAR PERFIL</h1>
							<p>Confira todas as informações em clique em salvar!</p>
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12" id="video_painel">
							<form action="" id="formulario">
								<h3 class="titulo_lab"><i class="fa fa-user"></i> INFORMAÇÕES PESSOAIS</h3>
								<label><input type="text" placeholder="NOME COMPLETO" value="HÉRCULES MOURA"></label>
								<label><input type="text" placeholder="COMO DESEJA SER CHAMADO" value="Galego"></label>
								<label><input type="text" placeholder="RUA" value="Vila nova da Rainha"></label>
								<label><input type="text" placeholder="BAIRRO" value="Centro"></label>
								<label class="cid"><input type="text" placeholder="CIDADE" value="Campina Grande"></label>
								<label class="uf"><input type="text" placeholder="ESTADO" value="PB"></label>
								<label><input type="email" placeholder="E-MAIL" value="nome@nome.com"></label>
								<label class="mL"><input type="text" placeholder="TELEFONE" value="(83) 0000.0000"></label>
								<label class="mR"><input type="text" placeholder="CELULAR" value="(83) 0000.0000"></label>

								<h3 class="titulo_lab"><i class="fa fa-unlock-alt"></i> INFORMAÇÕES DE ACESSO</h3>
								<label><input type="text" placeholder="USUÁRIO" value="hercules"></label>
								<label><input type="password" placeholder="SENHA"></label>
								<label><input type="password" placeholder="CONFIRME A SENHA"></label>

								<label><input type="checkbox"> Afirmo que todas as informações fornecidas por mim são verdadeiras.</label>

								<button><i class="fa fa-save"></i> SALVAR</button>
							</form>
						</div>
					</div><!-- ROW IN -->
				</div>
				<!-- PAINEL -->

				<?php include('notificacoes.php');?>

			</div>
		</div>
	</div>
	<!-- BOX PAGE -->

<?php
include('footer.php');
?>
