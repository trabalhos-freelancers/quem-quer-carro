<header class="section" id="head">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-4">
                <a href="/admin">
                    <img src="<?= $this->request->getAttribute('webroot') . 'img/logo.png' ?>" class="center-block img-responsive" alt="Quem quer carro?">
                </a>
            </div>
            <!--LOGO-->
            <div class="col-xs-12 col-sm-8 col-md-8 text-right" id="saudacao">
                <i class="fa fa-user"></i> Você está logado como
                <strong><?= $this->request->session()->read('Auth.User')['name'] ?></strong>

            </div>
            <!---->
        </div>
    </div>
</header>
<!--HEAD-->
