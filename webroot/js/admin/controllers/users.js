function deleteUser( id ) {
    swal({
        title: "Você tem certeza ?",
        text: "O Usuario será deletado da base de dados permanentemente",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            var form = $('#form-delete-user');
            url = form.attr('action') + '/' + id;
            form.attr('action', url);
            form.submit();
        }
    });
}

$(document).ready( function () {
    $('#users-table').DataTable({
        language: {
            url: '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json'
        }
    });
} );