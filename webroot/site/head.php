<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- Configurar nome e descrição de acordo com o que for definido no painel do Administrador -->
<title>Título do site - Descrição do site</title>

<meta name="description" content=" --Descrição aqui-- "/>
<meta property="og:title" name="og:title" content="--Titulo do site aqui--"/>
<meta property="og:description" content="--Descrição aqui--"/>

<meta name="keywords" content="--Palavras chaves aqui--"/>

<!-- FIM SEO -->


<link href="style.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.css" rel="stylesheet">

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

</head>
<body>
	<header class="section" id="head">
		<div class="container">
			<div class="row">
				
				<div class="col-xs-6 col-sm-4 col-md-4">
					<a href="index.php">
					<img src="img/logo.png" class="center-block img-responsive" alt="Quem quer carro?"></a>
				</div><!--LOGO-->

				<div class="col-xs-6 col-sm-8 col-md-8 text-right" id="acesso-topo">
					<button onclick="window.location.href='anunciar.php'">QUERO ANUNCIAR</button>
					<button data-toggle="modal" data-target=".login_topo" onclick="window.location.href='#'">LOGIN</button>
				</div><!---->

			</div>
		</div>
	
	</header><!--HEAD-->

	<nav id="menu">
		<div class="container">
			<div class="row">
				
				<div class="col-xs-12 col-sm-12 col-md-12">

		 <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-topo">
            <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
            <span class="sr-only">MENU</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         
        </div>

        <div class="collapse navbar-collapse" id="menu-topo">
        	<ul id="" class="nav navbar-nav navbar-center">
        		<li class="encontre"><a>ENCONTRE AQUI</a></li>
        		<li class="active"><a href="index.php">HOME</a></li>
				<li><a href="anuncios.php">CONCESSIONÁRIAS</a></li><!-- MOSTRAR ANUNCIOS DE CONCESSIONARIAS -->
				<li><a href="anuncios.php">PARTICULARES</a></li><!-- MOSTRAR ANUNCIOS PARTICULARES -->
				<li><a href="">OFICINAS</a></li>
				<li><a href="">SEGUROS</a></li>
				<li class="dropdown" data-toggle="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SERVIÇOS ESPECIALIZADOS <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="">SERVIÇO 1</a></li>
						<li><a href="">SERVIÇO 2</a></li>
						<li><a href="">SERVIÇO 3</a></li>
					</ul>
				</li>
        	</ul>

     
       </div>

   </div>
</div>
</div>
	</nav>
	<!-- MENU -->
	
