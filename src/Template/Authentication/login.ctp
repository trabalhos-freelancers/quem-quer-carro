<div class="container" style="margin-top: 12px;margin-bottom:-20px;">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-lg-8">
            <?= $this->Flash->render() ?>
        </div>
    </div>
</div>

<!-- LOGIN FORM -->
<div class="text-center" style="padding:50px 0">
    <div class="logo">
        <img src="../img/logo.png" alt="">
    </div>
    <!-- Main Form -->
    <div class="login-form-1">
        <form id="login-form" class="text-left" action="<?= \Cake\Routing\Router::url(['controller' => 'Login', 'action' => 'login', 'prefix' => 'authentication'])?>" method="POST">
            <div class="login-form-main-message"></div>
            <div class="main-login-form">
                <div class="login-group">
                    <div class="form-group">
                        <label for="lg_username" class="sr-only">Email</label>
                        <input type="text" class="form-control" id="lg_username" name="email" placeholder="Usuário" required>
                    </div>
                    <div class="form-group">
                        <label for="lg_password" class="sr-only">Senha</label>
                        <input type="password" class="form-control" id="lg_password" name="password" placeholder="Senha" required>
                    </div>
                    <div class="form-group login-group-checkbox">
                        <input type="checkbox" id="lg_remember" name="lg_remember">
                        <label for="lg_remember">Lembrar</label>
                    </div>
                </div>
                <button type="submit" class="login-button">
                    <i class="fa fa-chevron-right"></i>
                </button>
            </div>
            <div class="etc-login-form">
                <p>Esqueceu sua senha?
                    <a href="mudar-senha.php">Clique Aqui!</a>
                </p>
            </div>
        </form>
    </div>
    <!-- end:Main Form -->
</div>