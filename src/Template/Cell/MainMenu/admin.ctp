<nav id="menu">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-topo">
                        <i class="fa fa-bars fa-2x" aria-hidden="true"></i>
                        <span class="sr-only">MENU</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="menu-topo">
                    <ul id="" class="nav navbar-nav navbar-center">
                        <?php foreach( $menu as $item => $value ): ?>
                            <?php if ( gettype( $value ) == 'string' ): ?>
                                <li>
                                    <a href="<?= $value ?>"><?= $item ?></a>
                                </li>
                            <?php elseif ( gettype($value == 'array') ): ?>
                                <li class="dropdown" data-toggle="dropdown">
                                    <a href="#" 
                                        class="dropdown-toggle" 
                                        data-toggle="dropdown" 
                                        role="button" 
                                        aria-haspopup="true" 
                                        aria-expanded="false"
                                    >
                                        <?= $item ?>
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php foreach( $value as $subitem => $subroute ): ?>
                                            <li onclick="javascript:location.href='<?= $subroute ?>'">
                                                <a><?= $subitem ?></a>
                                            </li>
                                        <?php endforeach ?>
                                    </ul>
                                </li>
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>