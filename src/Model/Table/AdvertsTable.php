<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Adverts Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\ImagesTable|\Cake\ORM\Association\HasMany $Images
 *
 * @method \App\Model\Entity\Advert get($primaryKey, $options = [])
 * @method \App\Model\Entity\Advert newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Advert[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Advert|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Advert|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Advert patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Advert[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Advert findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AdvertsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('adverts');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Images', [
            'foreignKey' => 'advert_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug')
            ->add('slug', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('brand')
            ->maxLength('brand', 60)
            ->requirePresence('brand', 'create')
            ->notEmpty('brand');

        $validator
            ->scalar('model')
            ->maxLength('model', 60)
            ->requirePresence('model', 'create')
            ->notEmpty('model');

        $validator
            ->integer('year')
            ->requirePresence('year', 'create')
            ->notEmpty('year');

        $validator
            ->scalar('engine')
            ->maxLength('engine', 60)
            ->requirePresence('engine', 'create')
            ->notEmpty('engine');

        $validator
            ->scalar('color')
            ->maxLength('color', 30)
            ->requirePresence('color', 'create')
            ->notEmpty('color');

        $validator
            ->scalar('fuel')
            ->maxLength('fuel', 25)
            ->requirePresence('fuel', 'create')
            ->notEmpty('fuel');

        $validator
            ->numeric('value')
            ->requirePresence('value', 'create')
            ->notEmpty('value');

        $validator
            ->integer('km')
            ->requirePresence('km', 'create')
            ->notEmpty('km');

        $validator
            ->scalar('plate')
            ->maxLength('plate', 20)
            ->requirePresence('plate', 'create')
            ->notEmpty('plate');

        $validator
            ->scalar('resume')
            ->allowEmpty('resume');

        $validator
            ->boolean('air_conditioner')
            ->requirePresence('air_conditioner', 'create')
            ->notEmpty('air_conditioner');

        $validator
            ->boolean('eletric_glass')
            ->requirePresence('eletric_glass', 'create')
            ->notEmpty('eletric_glass');

        $validator
            ->boolean('airbag')
            ->requirePresence('airbag', 'create')
            ->notEmpty('airbag');

        $validator
            ->boolean('alarm')
            ->requirePresence('alarm', 'create')
            ->notEmpty('alarm');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['slug']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
