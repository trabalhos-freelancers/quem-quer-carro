<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <title>Quem quer Carro</title>


        <link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon" />
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        
        <!-- Bootstrap Core CSS -->
        <?= $this->Html->css('bootstrap.min.css?'.date('YmdHis')) ?>
        <?= $this->Html->css('font-awesome.css?'.date('YmdHis')) ?>
        
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

        <!-- Estilos do painel -->
        <?= $this->Html->css('admin/style-painel.css?'.date('YmdHis')) ?>
    </head>

    <body>
        <?= $this->element('header') ?>

        <!-- MENU -->
        <?= $this->cell('MainMenu::admin') ?>
        <!-- /MENU -->

        <!-- CONTENT -->
            <div class="container" style="margin-top: 12px;margin-bottom:-20px;">
                <div class="row">
                    <div class="col-sm-12 col-md-8 col-lg-8">
                        <?= $this->Flash->render() ?>
                    </div>
                </div>
            </div>
            <?= $this->fetch('content') ?>
        <!-- /CONTENT -->

        <!-- FOOTER -->
        <?= $this->element('footer') ?>
        <!-- /FOOTER -->

        <!-- Modal Login -->
        <?= $this->element('login') ?>
        <!-- /Modal Login -->


        <!-- jQuery -->
        <?= $this->Html->script('jquery.js?'.date('YmdHis')) ?>
        
        <!-- Bootstrap Core JavaScript -->
        <?= $this->Html->script('bootstrap.min.js?'.date('YmdHis') )?>

        <!-- DataTables -->
        <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

        <!-- Sweet Alert -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        
        <!-- App custom scripts-->
        <?= $this->fetch('js') ?>
    </body>
</html>
