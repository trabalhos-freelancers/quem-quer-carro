<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

/** Autenticacao */
Router::connect('/login', ['controller' => 'Login', 'action' => 'login', 'prefix' => 'authentication']);
Router::connect('/logout', ['controller' => 'Logout', 'action' => 'logout', 'prefix' => 'authentication']);

/** Painel administrativo para cada usuário */
Router::connect('/dashboard', ['controller' => 'Dashboard', 'action' => 'dashboard']);

Router::prefix('admin', function(RouteBuilder $adminRoutes) {
    /** painel administrativo */
    $adminRoutes->connect('/', ['controller' => 'Dashboard', 'action' => 'index']);

    /** perfil de usuario */
    $adminRoutes->connect('/profile', ['controller' => 'Profile', 'action' => 'edit']);

    /** rotas de gerenciamento de usuários */
    $adminRoutes->scope('/users', function (RouteBuilder $adminUsersRoutes) {
        $adminUsersRoutes->connect('/', ['controller' => 'Users']);
        $adminUsersRoutes->connect('/:action/*', ['controller' => 'Users']);
    });

    /** rotas de gerenciamento de anúncios */
    $adminRoutes->scope('/adverts', function (RouteBuilder $adminUsersRoutes) {
        $adminUsersRoutes->connect('/', ['controller' => 'Adverts']);
        $adminUsersRoutes->connect('/:action/*', ['controller' => 'Adverts']);
    });
});