<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\TableRegistry;

/**
 * Users Model
 *
 * @property \App\Model\Table\AdvertsTable|\Cake\ORM\Association\HasMany $Adverts
 * @property \App\Model\Table\ProfilesTable|\Cake\ORM\Association\HasMany $Profiles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Adverts', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasOne('Profiles', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('nickname')
            ->maxLength('nickname', 255)
            ->allowEmpty('nickname');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->requirePresence('slug', 'create')
            ->notEmpty('slug');

        $validator
            ->scalar('category')
            ->maxLength('category', 10)
            ->requirePresence('category', 'create')
            ->notEmpty('category');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    /**
     * Cria usuário e seu respectivo profile e retorna true ou false
     * caso o procedimento seja bem ou mal sucedido.
     * 
     * @param array data
     * @return boolean
     */
    public function addUser($data)
    {
        /** Criando usuário */
        $user = $this->newEntity();
        $user = $this->patchEntity($user, $data);

        /** acertar isso para criar um slug unico */
        $user->slug = $user->email;
        /*$user->password = (new \Cake\Auth\DefaultPasswordHasher)->hash($user->password);*/

        if ( $this->save( $user ) ) {
            /** cria profile do usuário */
            $profileTable = TableRegistry::get('Profiles');
            $userProfile = $profileTable->newEntity();
            $userProfile = $profileTable->patchEntity($userProfile, $data);
            $userProfile->user_id = $user->id;
            $profileTable->save( $userProfile );

            return true;
        }

        return false;
    }

    /**
     * Atualiza o usuário e seu respectivo profile com os dados passados
     * e retorna a instancia do model atualizado caso o procedimento seja bem sucedido.
     * 
     * @param array $data
     * @param int $user_id
     * @return boolean
     */
    public function updateUser($data, $user_id)
    {
        $profileTable = TableRegistry::get('Profiles'); 

        $user = $this->get( $user_id, ['contain' => ['Profiles']] );
        $profile = $profileTable->get( $user->profile->id );
        
        if ( $data['password'] == '' ) {
            unset( $data['password'] );
            unset( $data['password_confirmation'] );
        } /*else {
            $data['password'] = (new \Cake\Auth\DefaultPasswordHasher)->hash($data['password']);
        }*/

        $user = $this->patchEntity($user, $data);
        $profile = $profileTable->patchEntity($profile, $data);

        if ($profileTable->save( $profile ) && $this->save( $user )) {
            return true;
        }

        return false;
    }

    /**
     * Deleta o usuário e seu respectivo profile da base de dados
     * 
     * @param int $user_id
     * @return boolean
     */
    public function deleteUser($user_id)
    {
        $profileTable = TableRegistry::get('Profiles'); 

        $user = $this->get( $user_id, ['contain' => ['Profiles']] );
        $profile = $profileTable->get( $user->profile->id );

        if ( $user && $profile ) {
            $profileTable->delete( $profile );
            $this->delete( $user );

            return true;
        }

        return false;
    }
}
