<div class="modal fade login_topo" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="modal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <img src="<?= $this->request->webroot . 'img/logo_login.png'?>" class="center-block img-responsive" alt="Quem quer carro?">
            <h4>INFORMAÇÕES DE LOGIN</h4>
            <div class="input-group">
                <span class="input-group-addon" id="sizing-addon2">
                    <i class="fa fa-user"></i>
                </span>
                <input type="text" class="form-control" placeholder="Usuário" aria-describedby="sizing-addon2">

                <span class="input-group-addon" id="sizing-addon2">
                    <i class="fa fa-unlock-alt"></i>
                </span>
                <input type="password" class="form-control" placeholder="Senha" aria-describedby="sizing-addon2">
            </div>
            <div class="relembrar">
                <p>Esqueceu sua senha?
                    <a href="">Clique aqui</a>
                </p>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
