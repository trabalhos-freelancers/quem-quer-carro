<?php
use Migrations\AbstractMigration;

class CreateUsersTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');

        $table->addColumn('name', 'string');
        $table->addColumn('nickname', 'string', ['null' => true]);
        $table->addColumn('email', 'string');
        $table->addColumn('password', 'string');
        $table->addColumn('slug', 'string');
        $table->addColumn('category', 'string', ['limit' => 10]); // admin - personal - dealership | admin - pessoal/particular - concessionária
        $table->addColumn('created', 'datetime');
        $table->addColumn('updated', 'datetime', ['null' => true]);

        $table->addIndex(['name']);
        $table->addIndex(['nickname', 'email', 'slug'], ['unique' => true]);

        $table->create();
    }
}
