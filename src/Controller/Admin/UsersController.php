<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->layout = 'admin'; 
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->Users->find('all', [
            'contain' => ['Profiles','Adverts']
        ]);

        $type = 'USUÁRIOS';

        /**
         * Processar Filtro necessário para correta exibição
         * Será preciso refatorar essa lógica ? Creio que não. 
         * Mas estou aberto a sugestões
         */
        if ( $this->request->getQuery('type') ) {
            $type = $this->request->getQuery('type');
            
            $opts = ['contain' => ['Profiles', 'Adverts']];

            $query = [];

            switch ($type) {
                case 'admin':
                    $type = 'ADMINISTRADORES';
                    $query = ['category =' => 'admin'];
                    break;

                case 'personal':
                    $type = 'PARTICULARES';
                    $query = ['category =' => 'personal'];
                    break;

                case 'dealership':
                    $type = 'CONCESSIONÁRIAS'; 
                    $query = ['category =' => 'dealership'];    
                    break;
            }

            $users = $this->Users->find('all', $opts)->where( $query );
        }

        $this->set(compact('users', 'type'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Adverts', 'Profiles']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();

        if ($this->request->is('post')) { 
            if ($this->Users->addUser( $this->request->getData() )) {
                $this->Flash->success(__('Usuário criado com sucesso!'));
                return $this->redirect(['action' => 'index']);
            }
            
            $this->Flash->error(__('Ocorreu algum erro e o usuário não pôde ser salvo. Tente novamente.'));
        }

        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Adverts','Profiles']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            if (
                $this->Users->updateUser($this->request->getData(), $id)) {
                $this->Flash->success(__('O usuário foi atualizado.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O usuário não pôde ser atualizado. Tente novamente.'));
        }

        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        if ($this->Users->deleteUser($id)) {
            $this->Flash->success(__('O usuário foi deletado da base de dados.'));
        } else {
            $this->Flash->error(__('O usuário não pôde ser deletado. Tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
