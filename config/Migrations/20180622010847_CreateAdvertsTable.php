<?php
use Migrations\AbstractMigration;

class CreateAdvertsTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('adverts');

        $table->addColumn('user_id', 'integer'); // Advert belongs to User | User has many Adverts
        $table->addColumn('slug', 'string');
        $table->addColumn('title', 'string');
        $table->addColumn('brand', 'string', ['limit' => 60]);
        $table->addColumn('model', 'string', ['limit' => 60]);
        $table->addColumn('year', 'integer');
        $table->addColumn('engine', 'string', ['limit' => 60]);
        $table->addColumn('color', 'string', ['limit' => 30]);
        $table->addColumn('fuel', 'string', ['limit' => 25]);
        $table->addColumn('value', 'float');
        $table->addColumn('km', 'integer');
        $table->addColumn('plate', 'string', ['limit' => 20]);
        $table->addColumn('resume', 'text', ['null' => true]);
        $table->addColumn('air_conditioner', 'boolean', ['default' => false]);
        $table->addColumn('eletric_glass', 'boolean', ['default' => false]);
        $table->addColumn('airbag', 'boolean', ['default' => false]);
        $table->addColumn('alarm', 'boolean', ['default' => false]);
        $table->addColumn('created', 'datetime');
        $table->addcolumn('updated', 'datetime', ['null' => true]);

        $table->addIndex([
            'title', 
            'brand', 
            'model',
            'year',
            'engine',
            'color',
            'fuel',
            'km',
            'plate',
            'air_conditioner',
            'eletric_glass',
            'airbag',
            'alarm'
        ]);
        $table->addIndex('slug', ['unique' => true]);

        $table->addForeignKey('user_id', 'users', ['id']);

        $table->create();
    }
}
