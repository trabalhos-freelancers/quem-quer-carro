<?php
use Migrations\AbstractMigration;

class CreateImagesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('images');
        $table->addColumn('advert_id', 'integer'); // Advert belongs to User | User has one Advert
        $table->addColumn('name', 'string');
        $table->addColumn('path', 'string');
        $table->addColumn('created', 'datetime');
        $table->addColumn('updated', 'datetime', ['null' => true]);
        $table->addIndex(['name', 'path']);
        $table->addForeignKey('advert_id', 'adverts', ['id']);
        $table->create();
    }
}
