<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Adverts Controller
 *
 * @property \App\Model\Table\AdvertsTable $Adverts
 *
 * @method \App\Model\Entity\Advert[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AdvertsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $adverts = $this->paginate($this->Adverts);

        $this->set(compact('adverts'));
    }

    /**
     * View method
     *
     * @param string|null $id Advert id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $advert = $this->Adverts->get($id, [
            'contain' => ['Users', 'Images']
        ]);

        $this->set('advert', $advert);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $advert = $this->Adverts->newEntity();
        if ($this->request->is('post')) {
            $advert = $this->Adverts->patchEntity($advert, $this->request->getData());
            if ($this->Adverts->save($advert)) {
                $this->Flash->success(__('The advert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert could not be saved. Please, try again.'));
        }
        $users = $this->Adverts->Users->find('list', ['limit' => 200]);
        $this->set(compact('advert', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Advert id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $advert = $this->Adverts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $advert = $this->Adverts->patchEntity($advert, $this->request->getData());
            if ($this->Adverts->save($advert)) {
                $this->Flash->success(__('The advert has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The advert could not be saved. Please, try again.'));
        }
        $users = $this->Adverts->Users->find('list', ['limit' => 200]);
        $this->set(compact('advert', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Advert id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $advert = $this->Adverts->get($id);
        if ($this->Adverts->delete($advert)) {
            $this->Flash->success(__('The advert has been deleted.'));
        } else {
            $this->Flash->error(__('The advert could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
