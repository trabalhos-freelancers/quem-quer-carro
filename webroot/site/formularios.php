<!-- BUSCA Form -->
  <div class="modal fade" id="BuscaForm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content form">

        <div class="modal-body">

          <strong style="text-align:left;"><i class="fa fa-search"></i> FAÇA UMA BUSCA</strong>
    
          <div class="newsform">
            <form method="get" id="buscadesck" action="<?php bloginfo('home'); ?>/">
              <input type="text" class="input" value="O que você procura?" onClick="this.value='';" name="s" id="s" />
              <input type="submit" value="BUSCAR" />
            </form><!--BUSCAR-->
          </div>

          <button type="button" class="nao btn btn-info btn-md" id="sair">Fechar</button>
          
    <br class="cl">

        </div>
      </div>
      
    </div>
  </div>
</div><!--BUSCA Form-->

<!-- Contato Form -->
  <div class="modal fade" id="ContatoForm" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content form">

        <div class="modal-body">

          <strong><i class="fa fa-envelope-o"></i> ENTRE EM CONTATO CONOSCO</strong>
      		<p>Preencha todos os campo para nos enviar uma mensagem.</p>

      		<div class="newsform">
      			<?php echo do_shortcode('[contact-form-7 id="18" title="Contato"]');?>
      		</div>

      		<button type="button" class="nao btn btn-info btn-md" id="Fechar">Fechar</button>
      		
		<br class="cl">

        </div>
      </div>
      
    </div>
  </div>
</div><!--Contato Form-->



 <!-- Assinar NewsLetter -->
  <div class="modal fade" id="AssinarNews" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content form">

        <div class="modal-body">
          <a href="" class="logoform"></a>
          <strong>PODE ENTRAR QUE A CASA É SUA ;)</strong>
		      <p>Cadastre seu e-mail e fique sabendo primeiro das ofertas, dicas e muito mais.</p>
          
          <div class="newsform">
          <?php echo do_shortcode('[contact-form-7 id="17" title="NewsLetter"]');?>
          </div>

      		<button type="button" class="ja btn btn-info btn-md" id="Jasou">Já sou cadastrado.</button>

      		<button type="button" class="nao btn btn-info btn-md" id="NaoQuero">Não, Obrigado.</button>
      		
      		<br class="cl">

        </div>
      </div>
      
    </div>
  </div>
</div><!--NEWS LETTER Abrindo na Home-->

<!--APENAS NA HOME-->
<script>
$(document).ready(function(){
    // Show the Modal on load
    $("#AssinarNews").modal("show");
    
    // Fechar
    $("#Jasou").click(function(){
        $("#AssinarNews").modal("hide");
    });

    // Fechar
    $("#NaoQuero").click(function(){
        $("#AssinarNews").modal("hide");
    });

    // Abrir NewsLetter
    $("#Rodape").click(function(){
        $("#AssinarNews").modal("show");
    });

    // Contato Form
    $("#formcontato").click(function(){
        $("#ContatoForm").modal("show");
    });

    $("#Fechar").click(function(){
        $("#ContatoForm").modal("hide");
    });

    // Busca Form
    $("#busca").click(function(){
        $("#BuscaForm").modal("show");
    });

    $("#sair").click(function(){
        $("#BuscaForm").modal("hide");
    });
    
});
</script>